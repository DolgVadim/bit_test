<?php

require_once '../vendor/autoload.php';
$routes = require_once '../app/routes.php';

$uri = $_SERVER['REQUEST_URI'];
$method = $_SERVER['REQUEST_METHOD'];

$action = isset($routes[$method][$uri]) ? $routes[$method][$uri] : null;

if ($action == null) {
    http_response_code(404);
    die();
}

list($controllerName, $method) = explode('@', $action);

$controllerClass = '\\App\\Controllers\\' . $controllerName;

if (method_exists($controllerClass, $method)) {
    $controller = new $controllerClass;

    $result = $controller->$method();

    if (!empty($result))
        echo $result;

} else {
    http_response_code(404);
    die();
}
?>

