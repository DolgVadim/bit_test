<?php

namespace App\Services;

use mysqli;

class DB
{
    private $connection = null;

    public function __construct()
    {
        $dsn = "mysql:host=localhost;dbname=bit_test";
        $user = "bit_test";
        $passwd = "bit_test";

        $this->connection = new \PDO($dsn, $user, $passwd);
    }

    public function query($sql, $params=[])
    {
        $statement = $this->connection->prepare($sql);
        $statement->execute($params);
        return $statement->execute($params) ? $statement : false;
    }

    public function beginTransaction()
    {
        $this->connection->beginTransaction();
    }

    public function commit()
    {
        $this->connection->commit();
    }

    public function rollBack()
    {
        $this->connection->rollBack();
    }
}