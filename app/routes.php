<?php

namespace App\Controllers;

return [
    'GET' => [
        '/' => 'UserBalanceController@index',
        '/login' => 'AuthController@index',
    ],
    'POST' => [
        '/' => 'UserBalanceController@withdraw',
        '/login' => 'AuthController@login',
        '/logout' => 'AuthController@logout',
    ],
];