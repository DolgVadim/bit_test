<?php

namespace App\Controllers;

use App\Models\User;

class UserBalanceController
{

    public function index()
    {
        session_start();
        $userId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
        session_write_close();

        if ($userId == null)
            header('Location: /login');

        $user = new User($userId);

        include $_SERVER['DOCUMENT_ROOT'] . '/../app/views/balance.php';
    }

    public function withdraw()
    {
        session_start();
        $userId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
        session_write_close();

        $user = new User($userId);
        $user->withdraw();
        header('Location: /');
    }
}