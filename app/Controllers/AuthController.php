<?php

namespace App\Controllers;

use App\Models\User;

class AuthController
{

    public function index()
    {
        include $_SERVER['DOCUMENT_ROOT'] . '/../app/views/auth_form.php';
    }

    public function login()
    {
        list('login' => $login, 'password' => $password) = $_POST;
        $userId = User::authAttempt($login, $password);
        if ($userId) {
            session_start();
            unset($_SESSION['auth_error']);
            $_SESSION['user_id'] = $userId;
            session_write_close();
            header('Location: /');
        } else {
            session_start();
            $_SESSION['auth_error'] = 'Неверные данные';
            session_write_close();
            header('Location: /login');
        }
    }

    public function logout()
    {
        session_start();
        unset($_SESSION['user_id']);
        session_write_close();
        header('Location: /');
    }
}