<?php

namespace App\Models;

use App\Services\DB;
use Symfony\Component\Config\Definition\Exception\Exception;

class User
{
    static $db;
    private $id;
    private $balance;
    private $login;

    public function __construct($id)
    {
        $this->id = $id;
        $db = static::getDb();
        $res = $db->query("select * from users where id=:id", [':id' => $id]);

        if ($res) {
            list('balance' => $this->balance, 'login' => $this->login) = $res->fetch();
        } else
            throw new \Exception('user not found', 404);
    }

    static function authAttempt($login, $password)
    {
        $db = static::getDb();

        $res = $db->query("select * from users where login=:login and password=:password", [':login' => $login, ':password' => $password]);

        if ($res == false)
            return $res;

        $data = $res->fetch();

        return !empty($data) ? $data['id'] : false;
    }

    static function getDb(): DB
    {
        if (empty(static::$db))
            static::$db = new DB();

        return static::$db;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function withdraw()
    {
        $amount = $_POST['amount'];

        if ($amount <= 0)
            return;

        $balance = $this->balance - (int)$amount;
        $db = static::getDb();
        $db->beginTransaction();
        try {
            $db->query('select * from users where id=:id for update', [':id' => $this->id]);
            $db->query("update users set balance=:balance where id=:id", [':balance' => $balance, ':id' => $this->id]);
            $db->commit();
        } catch (Exception $exception) {
            $db->rollBack();
        }

    }

    public function getLogin()
    {
        return $this->login;
    }
}